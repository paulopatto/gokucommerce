class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :zipcode
      t.string :neighborhood
      t.string :city
      t.string :state, limit: 2
      t.string :number
      t.string :complement

      t.timestamps
    end
  end
end
