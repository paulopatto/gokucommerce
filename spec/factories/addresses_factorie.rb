FactoryGirl.define do
  factory :address do
    street "Av. Eng. Luís Carlos Berrini"
    zipcode "04571-000"
    neighborhood "Brooklin"
    city "São Paulo"
    state "SP"
    number "550"
    complement "8º e 13º Andar ao lado da caixa economica"
  end
end
