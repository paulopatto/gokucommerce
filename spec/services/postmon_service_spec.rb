require 'rails_helper'

describe PostmonService do
  let(:zipcode) { "01001000" }
  let(:absent_zipcode) { "05894232" }

  describe '#get' do
    subject { described_class.new.get(zipcode) }

    it 'returns hash', vcr: { cassette_name: 'get_postmon_cep' } do
      expect(subject).to be_kind_of(Hash)
    end

    context "when send valid :zipcode", vcr: { cassette_name: 'get_postmon_valid_cep' } do
      subject { described_class.new.get(zipcode) }

      it 'returns all address keys as symbol' do
        %w(
          street
          neighborhood
          street
          city
          state
          zipcode
        ).each do |key|
          expect(subject).to be_include(key.to_sym)
        end
      end
    end

    context 'when send absent :zipcode', vcr: { cassette_name: 'get_postmon_absent_cep' } do
      subject { described_class.new.get(absent_zipcode) }

      it 'also returns hash' do
        expect(subject).to be_kind_of(Hash)
      end

      it 'returns hash with error key' do
        expect(subject).to be_include(:error)
      end

      it 'return hash :error key with key :message value "address not found"'  do
        expect(subject[:error]).to be_include(:message)
        expect(subject[:error][:message]).to eq 'address not found'
      end
    end
  end
end
