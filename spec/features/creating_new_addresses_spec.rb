require 'rails_helper'

feature "Creates new address", type: :feature do

  before do
    VCR.turn_off!
    WebMock.allow_net_connect!
  end

  after do
    VCR.turn_on!
    WebMock.disable_net_connect!
  end

  let(:user) { create(:user) }
  let(:attrs_names_for_address) do
    %w(
      street
      neighborhood
      street
      city
      state
      number
      zipcode
      complement
    )
  end
  let(:address_attrs) { build(:address) }

  scenario 'Redering form new_address' do
    login_as(user, :scope => :user)

    visit new_address_path

    expect(page).to have_css("form#new_address")
    attrs_names_for_address.each do |attr_name|
      expect(page).to have_css("form#new_address input[name='address[#{attr_name}]']")
    end
  end

  scenario 'Create new address: BrasilCT HQ' do
    login_as(user, :scope => :user)

    visit new_address_path

    fill_in 'address[street]',     with: address_attrs.street
    fill_in 'address[zipcode]',    with: address_attrs.zipcode
    fill_in 'address[number]',     with: address_attrs.number

    click_on "Create Address"

    address = Address.where(zipcode: address_attrs.zipcode).take

    expect(address.street).to eq address_attrs.street
  end
end
