require 'rails_helper'

describe Address, type: :model do
  describe ".by_cep" do
    let(:cep) { '05894230' }
    let(:absent_cep) { '05894232' }

    context 'when send valid cep', vcr: { cassette_name: 'model_address_by_cep' } do
      subject { described_class.by_cep(cep) }

      it 'returns new address instance' do
        expect(subject).to be_kind_of Address
      end

      it 'returns correct street name' do
        expect(subject.street).to eq 'Rua Agostinho de Paiva'
      end
    end

    context 'when send absent cep', vcr: { cassette_name: 'model_absent_cep' } do
      subject { described_class.by_cep(absent_cep) }

      it 'returns nil' do
        expect(subject).to be_nil
      end
    end
  end
end
