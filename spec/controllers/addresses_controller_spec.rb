require 'rails_helper'

describe AddressesController, type: :controller do
  let(:user) { create(:user) }

  describe "GET #new" do

    before do
      sign_in user
      get :new
    end

    it 'returns with :success' do
      expect(response).to be_success
    end

    it { expect(response).to render_template(:new) }

    it 'assings new empty address' do
      expect(assigns(:address)).to be_a_new(Address)
    end
  end

  describe "POST #create" do
    let(:address_params) {
      {
        street: "Av. Maria Coelho Aguiar",
        number: "215",
        neighborhood: "Jardim São Luís",
        city: "São Paulo",
        state: "SP",
        zipcode: "05804900"
      }
    }

    before do
      sign_in user
      post :create, params: { address: address_params }
    end

    it 'creates new address record on database' do
      address = Address.find_by(zipcode: address_params[:zipcode])

      expect(address).to_not be_nil
      expect(address.street).to eq address_params[:street]
    end

    it 'action redirects to :show created :address' do
      expect(response).to redirect_to action: :show, id: assigns(:address).id
    end
  end

  describe '#address_by_cep', vcr: { cassette_name: "addresses_controller_address_by_cep" } do
    let(:cep) { '05894230' }

    before do
      sign_in user
      get :address_by_cep, params: { cep: cep }
    end

    it 'responds with success' do
      expect(response).to be_succes
    end

    it 'returns json with correct address' do
      body = JSON.parse(response.body)

      expect(body['street']).to be_include "Agostinho de Paiva"
      expect(body['state']).to eq "SP"
    end
  end

  describe '#list_by_cep' do
    let(:cep) { '05804900' }
    let(:quantity) { rand(5...50) }

    before do
      quantity.times.each do |number|
        create(:address, zipcode: cep, number: number, complement: "Room ##{number}")
      end
      sign_in user

      get :list_by_cep, params: { cep: cep }
    end

    it 'render template :index' do
      expect(response).to render_template :index
    end

    it 'assigns :addresses with all address with query :cep' do
      expect(assigns(:addresses).count).to eq(quantity)
    end
  end
end
