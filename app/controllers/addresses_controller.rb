class AddressesController < ApplicationController
  before_action :authenticate_user!

  def index
    @addresses = Address.all
  end

  # POST /addresses/create
  def create
    @address = Address.create(address_params)
    redirect_to address_path(@address)
  end

  # GET /addresses/new
  def new
    @address = Address.new
  end

  def show
    @address = Address.find(id)
  end

  def list_by_cep
    @addresses = Address.where(zipcode: cep)
    render :index
  end

  def address_by_cep
    @address = Address.by_cep(cep)
    render json: @address
  end

  private

  def address_params
    #TODO: Add filter to zipcode returns only digits. See #cep
    params.require(:address).permit(
      :neighborhood,
      :street,
      :city,
      :state,
      :number,
      :zipcode,
      :complement
    )
  end

  def id
    params.require(:id)
  end

  def cep
    params.require(:cep).scan(/\d/).join('')
  end
end
