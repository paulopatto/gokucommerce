module Admin
  class UsersController < ApplicationController
    before_action :load_user, only: [:destroy]

    def index
      @users = User.all
    end

    def destroy
      @user.destroy

      redirect_to users_path
    end

    private

    def load_user
      @user = User.find(id)
    end

    def id
      params.require(:id)
    end
  end
end
