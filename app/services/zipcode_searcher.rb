module ZipcodeSearcher
  def get(zipcode)
    search(zipcode)
  end

  private

  def translate_address(origin_hash, translate_table)
    origin_hash.inject({}) do |memo, (k,v)|
      memo[translate_table[k.to_sym]] = v unless translate_table[k.to_sym].nil?
      memo
    end
  end
end
