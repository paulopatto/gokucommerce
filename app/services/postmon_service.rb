class PostmonService
  include HTTParty
  include ZipcodeSearcher

  base_uri 'http://api.postmon.com.br/v1'

  # O serviço postmons envia os dados com as chaves:
  #
  # - logradouro
  # - bairro
  # - cidade
  # - cep
  # - estado
  #
  # - estado_info
  # -- area_km2
  # -- codigo_ibge
  # -- nome
  #
  # - cidade_info
  # -- area_km2
  # -- codigo_ibge
  #
  # E o sistema entende para endereços:
  #
  # - street
  # - zipcode
  # - neighborhood
  # - city
  # - state
  # - number
  # - complement
  TRANSLATE_TABLE = {
    logradouro: :street,
    bairro: :neighborhood,
    cidade: :city,
    cep: :zipcode,
    estado: :state
  }

  private

  def search(zipcode)
    response = self.class.get "/cep/#{zipcode}"
    return { error: { message: 'address not found' } } if response.code == 404

    address = JSON.parse( response.body )
    translate_address(address, TRANSLATE_TABLE)
  end
end
