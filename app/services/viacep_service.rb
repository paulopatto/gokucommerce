class ViacepService
  include HTTParty
  include ZipcodeSearcher

  base_uri 'https://viacep.com.br/ws/'

  # O serviço viacep envia os dados com as chaves:
  #
  # - cep
  # - logradouro
  # - complemento
  # - bairro
  # - localidade
  # - uf
  # - unidade
  # - ibge
  # - gia
  #
  # E o sistema entende para endereços:
  #
  # - street
  # - zipcode
  # - neighborhood
  # - city
  # - state
  # - number
  # - complement
  TRANSLATE_TABLE = {
    logradouro: :street,
    bairro: :neighborhood,
    localidade: :city,
    cep: :zipcode,
    uf: :state,
    complemento: :complement
  }

  private

  def search(zipcode)
    response = self.class.get "/#{zipcode}/json"
    return { error: { message: 'address not found' } } if JSON(response.body).include? "erro"
    address = JSON.parse( response.body )
    translate_address(address, TRANSLATE_TABLE)
  end
end
