class Address < ApplicationRecord

  #TODO: Improve this code
  def self.by_cep(cep)
    provider = [ViacepService, PostmonService].sample.new
    address_attrs = provider.get(cep)

    return nil if address_attrs.include? :error

    Address.new(address_attrs)
  end
end
