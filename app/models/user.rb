class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable,
  # :recoverable, :rememberable, :trackable, :validatable and :omniauthable
  devise :database_authenticatable, :registerable
end
