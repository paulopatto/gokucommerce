// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(function() {
  $('form#new_address').formValidation({
    framework: 'bootstrap',
    locale: 'pt_BR',

    fields: {
      'address[zipcode]': { validators: { notEmpty: {} } },
      'address[street]': { validators: { notEmpty: {} } },
      'address[number]': { validators: { notEmpty: {} } }
    }

  });
});
