Rails.application.routes.draw do

  root to: 'addresses#index'

  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :addresses do
    collection do
      get 'cep/:cep', action: 'address_by_cep', defaults: { format: 'json' }
      get 'by_cep/', action: 'list_by_cep'
    end
  end

  scope :admin do
    resources :users, only: [:show, :index, :destroy], controller: 'admin/users'
  end
end
