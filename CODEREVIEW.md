## Eu como dono da Goku e commerce ...

- [X] necessito de um cadastro de endereços.
    > Objetivo alcançado com a url `GET /addresses/new` e a ação `POST /addressess/`
- [X] necessito de uma página de consulta de endereços através do CEP.
    > Este objetivo é dado como parcialmente alcançado, pois eu não criei uma nova rota
    > de pesquisa de novos endereços e sim coloquei um campo de bsuca no topo da página
    > que dado que um endereço está cadastrado com um cep ele será localizado. Isso é
    > entregue através da rota `GET /addresses/by_cep`. Neste caso essa busca não é bem trabalhada,
    > pois além desse fomrulário poder usar uma ação por requisição assincrona (AJAX) eu poderia ter
    > criado algo como um presenter/view para essa vizualização. Mas não o fiz.
    >
    > Porém, toda via, entre tanto eu não sabia se essa busca era apenas uma busca simples
    > de active record (rails básico sôh), ai eu pensei que poderia ser algo mais interessante
    > como aqueles campos de auto preencher endereço ataravés do CEP. Coisa que é bem mais legal.
    > Fora que isso mostra mais dominio com a linguagem ruby e até com o FW Rails.
    > Criei o tipo `ZipcodeSearrch` [app/services/zipcode_searcher.rb] que é uma interface
    > para serviços de busca de CEP, assim posso extender meus fornecedores de endereços por cep
    > como base de dados, APIs ou fadas madrinhas. E implementei dois clients de apis abertas de cep
    > :postmon e :viacep, e uso eles de forma transparente através dessa interface,
    > que é demonstrado com o sorteio de client usado na busca de cep com Array#sample em:
    > ```ruby
    > # app/models/address.rb
    > def self.by_cep(cep)
    >   provider = [ViacepService, PostmonService].sample.new
    >   address_attrs = provider.get(cep)
    >   ...
    > end
    > ```
    > A rota `GET /addresses/cep/:cep` foi criado com o intuito de fornecer os dados para
    > o ajax/jquery de completar endereço através do CEP com o uso dessas apis, mas tempo e motivos
    > já explanados fizeram com que isso não esteja implementado.
- [X] necessito de um cadastro de usuários para manter o cadastro de endereço.
    > Isso ai é com a gem devise
- [X] necessito de uma página de administração com login para os usuários cadastrados.
    > Uma administração básica e rudimentar está com o escopo :admin e as rotas:
    > - GET    /admin/users
    > - GET    /admin/users/:id
    > - DELETE /admin/users/:id
    > Onde você só verifica a lista de usuários cadastrados, exibe um e deleta um.

Após uma analise sistemica do PO juntamente com a equipe de desenvolvimento,
eles chegaram aos seguintes requisitos técnicos:

- De modo a atender uma necessidade futura da loja Goku, que é
atuar tanto na web como em plataformas mobile, todos os serviços
devem atender as duas frentes de maneira que não gere nem um
impacto nas vendas.
> NÃO CONCLUIDO, o frontend fico um lixo, tentei usar o layout responsivo com o bootstrap
> mas não rolo e eu perti um tempo enorme que não podia com isso.
> Realmente aqui eu falhei miseravelmente.

- De maneira a atender a necessidade de performance do sistema
todos esses serviços devem estar cobertos por uma estrutura de
cache.
> Esse requisito ou eu subia um memcache ou usava o cache de memória.
> Seria apenas alguma receita do manual http://edgeguides.rubyonrails.org/caching_with_rails.html
> Ou o cache com o redis que talvez faça mais sentido com instalação do redis e config:
> ```ruby
> # config/application.rb
> config.cache_store = :redis_store, 'redis://localhost:6379/0/cache', { expires_in: 90.minutes }
> ```

- De maneira a garantir a segurança das informações esses
serviços devem possuir alguma solução para garantir que não possam
ser acessados por pessoas ou sistemas não autorizados.
> Para isso as rotas usam autenticação com devise, até a rota de api de busca de cep
> quando você não está autenticado te retorna um:
> `{"error":"You need to sign in or sign up before continuing."}`.

-  Para garantir a performance na web chegamos a conclusão de que
nem uma biblioteca do tipo JSF ou similar deve ser utilizada para o
desenvolvimento do frontend, mas também não quer dizer que as
paginas não deverão possuir um design agradável.
> Poxa pego no ponto fraco, usei o bootstrap, então aqui o objetivo não foi concluido.

- O frontend deve ser desenvolvido de forma que a renderização
fique do lado do cliente.
> Dessa forma eu deveria ter desenvolvido apenas uma api de backend e usado
> o reactjs para e feito um SPA.


## Outros pontos a comentar.

#### Suite de testes

A suite de testes passa, mas está anos luz de ser uma suite de testes ideal, pois
tem apenas caminho feliz. Acho que os testes que mais se aproximam de uma coisa mais decente são os dos services
de busca de cep, pois não testam apenas o caminho feliz ao que me lembro.


#### Versionamento

Como metodologia de trabalho com git sempre uso o modelo de branchs proposto em http://nvie.com/posts/a-successful-git-branching-model/
e uma ferramenta bem legal para isso é o [git-flow](https://github.com/petervanderdoes/gitflow-avh) que foi usado no inicio e só fugi dele para escrever esse doc e o deploy.

#### Commit tags and prepare-commit-msg

Sempre me perguntam do por que de todo commit ter esse `[nome do branch]`, e isso é por causa do https://github.com/paulopatto/dotfiles/blob/master/git/hooks/prepare-commit-msg.

#### Sobre a entrega

Realmente eu gostaria de ter feito um trabalho bem melhor, mas por motivos pessoais eu não consegui me focar nessa atividade,
peço sinceras desculpas quanto a isso. Não entenda que eu dei pouca importância a atividade mas tive me desdobrar com
arranjar uma máquina para desenvolver, acesso a internet (pontos que citei em conversa pessoal no dia em que fui ao escritório) e
uma infortuna issue prioritária que é o Marcus ![Marcus](https://scontent.fgru3-1.fna.fbcdn.net/v/t1.0-0/s526x395/14079648_1205808269469929_7960271423414452583_n.jpg?oh=d3abf2bd4d6988a04e2a50b3a0bb1236&oe=5846F1F5 "Marcus")
Acredito que vale a pena dar uma olhadela no github ou ver no linkedin quem pode advogar por mim, quanto ao meu trabalho.
Acabei tentando agora de madrugada finalizar algo, mas estou muito cansado. Vou tentar dormir umas 3 horas antes de voltar para o Hospital.
Independente da avaliação deste repositório esse projeto vai sofrer uma reformulação e ficar disponível aberto no bitbucket com uma stack diferente (depois que eu desafogar e de terminar de ler o livro "React Guia do Iniciante" - https://leanpub.com/react-guia-do-iniciate.


Abraços.
Paulo Patto
