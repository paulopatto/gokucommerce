# Livro de endereços

> ~~Faz mais sentido do que gokucommerce~~

O projeto está rodando no heroku e pode ser acessado em http://goku-commerce-brasil-ct.herokuapp.com/.

Este é um projeto Rails simples, então é so fazer o chekout do código e rodar o comando `bundle install`.
Os códigos foram desenvolvidos sob a versão 2.3.0 da linguagem (como pode ser visto pelo arquivo `.ruby-version`,
mas em produção está rodando sem problemas sobre a versão 2.1.2 do interpretador, o framework base (Rails 5) tem suporte oficial a ruby 2.2.2 conforme https://github.com/rails/rails/blob/v5.0.0/rails.gemspec.

### Antes de rodar

Antes de rodar o projeto com o comando `rails server` você precisa montar o banco de dados SQLite3 (em desenvolvimento) com o comando `rake db:migrate` e precisa baixar as libs javascript que foram usadas, para isso usei o gerenciador de dependências [bower](https://bower.io/), então se você já tem o nodejs na sua máquina pode instalar o bower com: `npm install bower -g` e ai você pode baixar as dependências com: `bower install`.

### Suite de testes

Esse projeto usa o RSpec como lib de testes com o capybara com o PhantomJS. Por isso para rodar os seus testes você precisa instalar o PahntomJS, não vou me alongar aqui sobre instalação deste, então viste http://phantomjs.org/ para mais informações.

### Heroku

Para usar o heroku eu tive de recorrer a um buildpack especial para poder rodar o bower install antes do assets:precompile. Assim para rodar esse projeto no heroku você precisa do buildpack https://github.com/qnyp/heroku-buildpack-ruby-bower.
